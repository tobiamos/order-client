import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';
import { sync } from 'vuex-router-sync';

import Vue from 'vue';
import BootstrapVue from 'bootstrap-vue';
import App from './App.vue';
import router from './router';
import store from './store';
import TokenHelper from './services/Token';


Vue.config.productionTip = false;
Vue.use(BootstrapVue);
sync(store, router);


const tokenHelper = new TokenHelper();

router.beforeEach((to, from, next) => {
  if (to.matched.some(route => route.meta.requiresAuth)) {
    if (!store.state.token || tokenHelper.isTokenExpired(store.state.token, 0)) {
      return next({
        path: '/login',
        params: { nextUrl: to.fullPath },
      });
    }
  } else if (to.matched.some(route => route.meta.guest)) {
    if (!store.state.token || tokenHelper.isTokenExpired(store.state.token, 0)) {
      return next();
    }
    return next({ name: 'Home' });
  }
  return next();
});

new Vue({
  render: h => h(App),
  router,
  store,
}).$mount('#app');
