import Api from '@/services/Api';

export default {
  getProducts() {
    return Api().get('/product/');
  },
  placeOrder(payload) {
    return Api().post('/order', payload);
  },
  getHistory() {
    return Api().get('/order');
  },
  getOrder(id) {
    return Api().get(`/order/${id}`);
  },
};
