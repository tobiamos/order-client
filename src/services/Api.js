import axios from 'axios';
import store from '@/store/';

export default () => axios.create({
  baseURL: 'https://agile-journey-62760.herokuapp.com/api/v1',
  headers: {
    Authorization: store.state.token,
  },
});
