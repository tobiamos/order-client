import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from 'vuex-persistedstate';

Vue.use(Vuex);

export default new Vuex.Store({
  strict: true,
  plugins: [
    createPersistedState(),
  ],
  state: {
    token: null,
    user: null,
    isUserLoggedIn: false,
    orders: [],
    total: 0,
  },
  mutations: {
    setToken(state, token) {
      state.token = token;
      state.isUserLoggedIn = !!(token);
    },
    setUser(state, user) {
      state.user = user;
    },
    addOrder(state, order) {
      state.orders = [order, ...this.orders];
    },
    removeOrder(state, name) { //eslint-disable-line
      const index = this.state.orders.findIndex(el => el.name === name);
      if (index !== -1) {
        this.orders.splice(index, 1);
        return this.getTotal();
      }
    },
  },
  actions: {
    setToken({ commit }, token) {
      commit('setToken', token);
    },
    setUser({ commit }, user) {
      commit('setUser', user);
    },
    addOrder({ commit }, order) {
      commit('addOrder', order);
    },
    removeOrder({ commit }, name) {
      commit('removeOrder', name);
    },
  },
});
