import Vue from 'vue';
import Router from 'vue-router';
import Register from '@/components/Register.vue';
import Login from '@/components/Login.vue';
import History from '@/components/History.vue';
import Order from '@/components/Order.vue';
import Home from '@/components/Home.vue';
import Detail from '@/components/Detail.vue';

Vue.use(Router);

export default new Router({
  routes: [{
    path: '/register',
    name: 'Register',
    component: Register,
    meta: {
      guest: true,
    },
  },
  {
    path: '/login',
    name: 'Login',
    component: Login,
    meta: {
      guest: true,
    },
  },
  {
    path: '/history',
    name: 'History',
    component: History,
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: '/history/:orderId',
    name: 'Detail',
    component: Detail,
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: '/order',
    name: 'Order',
    component: Order,
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: '/home',
    name: 'Home',
    component: Home,
  },
  {
    path: '*',
    redirect: 'home',
  },

  ],
});

